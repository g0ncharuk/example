/**
 * @author g0ncharuk
 */

'use strict';

angular.module('BlurAdmin', [
    'ngAnimate',
    'ui.bootstrap',
    'ui.sortable',
    'ui.router',
    'permission',
    'permission.ui',
    'ngTouch',
    'toastr',
    'smart-table',
    "ui.tinymce",
    "xeditable",
    'ui.slimscroll',
    'ngJsTree',
    'angular-progress-button-styles',

    'BlurAdmin.theme',
    'BlurAdmin.pages'
]);
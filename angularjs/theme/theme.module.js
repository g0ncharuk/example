/**
 * @author g0ncharuk
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.theme', [
      'toastr',
      'ngMask',
      'chart.js',
      'angular-chartist',
      'angular.morris-chart',
      'textAngular',
      'angularFileUpload',
      'BlurAdmin.theme.components',
      'BlurAdmin.theme.inputs'
  ]);

})();

/**
 * @author v.lugovsky
 * created on 10.12.2016
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.theme.inputs')
        .directive('fileUploader', fileUploader);

    /** @ngInject */
    function fileUploader() {
        return {
            templateUrl: 'app/theme/inputs/fileUploader/fileUploader.html',
            scope: {
                fuModel: "=$files",
                fuPreview: "=",
                fuWidth: "@",
                fuHeight: "@",
                fuAccept: "@",
                fuMultiple: "@"
            },


            link: function (scope, elem, attrs) {
                var img = new Image();
                scope.multItemLength = 0;

                if (JSON.parse(scope.fuMultiple)) {
                    elem.find('input[type=file]')[0].setAttribute('multiple', 'multiple')
                }

                scope.filterFunc = function (val) {
                    return JSON.parse(val)
                };

                elem.on("change", function (e) {
                    scope.$parent.$eval(attrs.fuModel + "=$files", {$files: e.target.files});
                    scope.$apply();
                    if (e.target.files[0]) {
                        if (!JSON.parse(scope.fuMultiple)) {
                            img.src = URL.createObjectURL(e.target.files[0]);
                            var reader = new FileReader();
                            reader.readAsDataURL(e.target.files[0]);
                            reader.onload = onLoadFile;
                        } else {
                            var res = elem.find("#image-preview");
                            res.html("");
                            scope.multItemLength =e.target.files.length;
                            _.forEach(e.target.files, function (item) {
                                var reader = new FileReader();
                                reader.readAsDataURL(item);
                                reader.onload = onLoadFileMult;
                            })
                        }
                    }

                    function onLoadFile(event) {
                        var img = new Image();
                        img.src = event.target.result;
                        img.onload = drowImageFill;
                        scope.fuPreview = '';
                    }

                    function onLoadFileMult(event) {
                        var res = elem.find("#image-preview");
                        res.append("<div><img height='"+ scope.fuHeight/2 +"' src='" + event.target.result + "'/><div>");
                    }
                });

                function drowImageFill() {
                    var width, height, offset;
                    var canvas = elem.find('canvas')[0];
                    var ctx = canvas.getContext('2d');
                    ctx.clearRect(0, 0, canvas.width, canvas.height);
                    if (img.height / img.width * canvas.width > canvas.height) {
                        width = canvas.width;
                        height = img.height / img.width * width;
                        offset = (height - canvas.height) / 2;
                        ctx.drawImage(img, 0, -offset, width, height);
                    } else {
                        height = canvas.height;
                        width = img.width / img.height * height;
                        offset = (width - canvas.width) / 2;
                        ctx.drawImage(img, -offset, 0, width, height);
                    }
                }
            }
        };
    }
})();

/**
 * @author g0ncharuk
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.theme')
        .run(themeRun);

    /** @ngInject */
    function themeRun($timeout, $window, $rootScope, $state, layoutPaths, preloader, $q,
                      PermRoleStore, PermPermissionStore, $http, $urlRouter, baSidebarService) {


        PermPermissionStore.definePermission('authorized', function () {
            if (JSON.parse(localStorage.getItem('authenticated'))) {
                return true;
            } else {
                $state.go('authorization');
                return false;
            }
        });

        PermRoleStore.defineRole('AUTHORIZED', ['authorized']);
        PermRoleStore.defineRole('handle_admin', function () {
            return !!JSON.parse(localStorage.getItem('handle_admin'));
        });
        PermRoleStore.defineRole('handle_album', function () {
            return !!JSON.parse(localStorage.getItem('handle_album'));
        });

        $rootScope.$on('$stateChangeStart', function (event, toState) {
            var authorized = JSON.parse(localStorage.getItem('authenticated'));
            var handle_admin = JSON.parse(localStorage.getItem('handle_admin'));
            var handle_album = JSON.parse(localStorage.getItem('handle_album'));
            if (authorized) {
                if (toState.name === "authorization") {
                    event.preventDefault();
                    $state.go('main.greeting');
                    return true;
                }
                if (toState.name === "main.users" && !handle_admin) {
                    event.preventDefault();
                    $state.go('main.greeting');
                    return true;
                }
                if (toState.name === "main.albums" && !handle_album) {
                    event.preventDefault();
                    $state.go('main.greeting');
                    return true;
                }

            }
            else {
                if ($state.is('authorization')) {
                    return false;
                } else {
                    if (toState.name !== "authorization") {
                        event.preventDefault();
                        $state.go('authorization');
                        return true;
                    }
                }
            }
        });

        var whatToWait = [
            preloader.loadAmCharts(),
            $timeout(3000)
        ];
        $q.all(whatToWait).then(function () {
            $rootScope.$pageFinishedLoading = true;
        });
        $timeout(function () {
            if (!$rootScope.$pageFinishedLoading) {
                $rootScope.$pageFinishedLoading = true;
            }
        }, 7000);
        $rootScope.$baSidebarService = baSidebarService;
    }

})();
/**
 * @author g0ncharuk
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.theme')
        .service('httpDataService', httpDataService)
        .service('dataService', dataService);

    /** @ngInject */
    function httpDataService($http, $q) {
        return {
            requestData: requestData
        };

        function requestData(data) {
            var isObject = angular.isObject;
            var isUndefined = angular.isUndefined;
            var toString = Object.prototype.toString;
            var form = new FormData();
            var iterator = new RecursiveIterator(data.body, 0, true);

            function getType(any) {
                return toString.call(any).slice(8, -1);
            }
            function toName(path) {
                var array = _.map(path, function (part) {
                    return "[" + part + "]";
                });
                array[0] = path[0];
                return array.join("");
            }

            if (!isObject(data)) {
                throw new TypeError("Argument must be object");
            }

            var appendToForm = function appendToForm(path, node, filename) {
                var name = toName(path);
                if (isUndefined(filename)) {
                    form.append(name, node);
                } else {
                    form.append(name, node, filename);
                }
            };

            iterator.onStepInto = function(_ref) {
                var parent = _ref.parent,
                    node = _ref.node;

                var type = getType(node);
                switch (type) {
                    case "Array":
                        return true; // step into
                    case "Object":
                        return true; // step into
                    case "FileList":
                        return true; // step into
                    default:
                        return false; // prevent step into
                }
            };

            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (
                    var _iterator = iterator[Symbol.iterator](), _step;
                    !(_iteratorNormalCompletion = (_step = _iterator.next()).done);
                    _iteratorNormalCompletion = true
                ) {
                    var _step$value = _step.value,
                        node = _step$value.node,
                        path = _step$value.path;

                    var type = getType(node);
                    switch (type) {
                        case "Array":
                            break;
                        case "Object":
                            break;
                        case "FileList":
                            break;
                        case "File":
                            appendToForm(path, node);
                            break;
                        case "Blob":
                            appendToForm(path, node, node.name);
                            break;
                        default:
                            appendToForm(path, node);
                            break;
                    }
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            // var formData = new FormData();
            // _.forEach(data.body, function (val, key1) {
            //     // console.log(1, val)
            //     if (_.isArray(val)) {
            //         // console.log(2, val)
            //         _.forEach(val, function (val, key2) {
            //             if (_.isObject(val) && val.length) {
            //                 // console.log(3, val)
            //                 _.forEach(val, function (val, key3) {
            //                     // console.log(4, val)
            //                     val !== undefined && val !== null ? formData.append(key1 + '[' + key2 + ']' + '[' + key3 + ']', val) : null;
            //                 })
            //             } else {
            //                 // console.log(5, val,_.isArray(val))
            //                 if (_.isArray(val)) {
            //                     console.log(7, val, typeof val)
            //                     _.forEach(val, function (val, key3) {
            //                         val !== undefined && val !== null ? formData.append(key1 + '[' + key2 + ']' + '[' + key3 + ']', val) : null;
            //                     })
            //                 } else {
            //                     if (_.isObject(val) && val.length) {
            //                         console.log(3, val)
            //                         _.forEach(val, function (val, key3) {
            //                             console.log(4, val)
            //                             val !== undefined && val !== null ? formData.append(key1 + '[' + key2 + ']' + '[' + key3 + ']', val) : null;
            //                         })
            //                     } else {
            //                         val !== undefined && val !== null ? formData.append(key1 + '[' + key2 + ']', val) : null
            //                     }
            //
            //                 }
            //             }
            //         })
            //     } else {
            //         // console.log(6, val)
            //         val !== undefined && val !== null ? formData.append(key1, val) : null
            //     }
            // });

            var request = $http({
                method: 'POST',
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: angular.identity,
                url: data.url,
                data: form
            });
            return (request.then(handleSuccess, handleError));
        }

        function handleSuccess(response) {
            return (response.data);
        }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }
    }

    function dataService() {
        var _editID = null,
            _canAdd = null;
        this.editID = _editID;
        this.canAdd = _canAdd;
    }

})();

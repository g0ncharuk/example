/**
 * @author g0ncharuk
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.home')
        .controller('HomeCtrl', HomeCtrl);

    /** @ngInject */
    function HomeCtrl($scope, $state, toastr, baProgressModal, $uibModal, dataService, httpDataService, Config) {
        var vm = this;

        vm.editAction = editAction;
        vm.fillForm = fillForm;
        vm.urlTransform = urlTransform;

        vm.getData = getData;
        vm.token = localStorage.getItem('token');

        function urlTransform(url) {
            var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
            var match = url.match(regExp);
            return (match&&match[7].length==11)? "https://www.youtube.com/embed/" + match[7] : url;
        }

        vm.getData();

        function getData() {
            var requestData = {
                "url": Config.getHome,
                "body": {
                    "token": vm.token
                }
            };

            baProgressModal.open();
            httpDataService.requestData(requestData).then(function (res) {
                if (res.status === 'success') {
                    baProgressModal.close();
                    vm.responseData = res.body;
                    vm.fillForm(vm.responseData)
                }
            }, function (error) {
                baProgressModal.close();
                toastr.error('Неудачно!')
            });
        }

        function fillForm(item) {
            vm._title = item.title;
            vm._subtitle = item.subtitle;
            vm._video = item.video;
            vm._description = item.description;
        }

        function editAction() {
            var requestData = {
                "url": Config.updateHome,
                "body": {
                    "token": vm.token,
                    "title": vm._title,
                    "subtitle": vm._subtitle,
                    "video": vm.urlTransform(vm._video),
                    "description": vm._description
                }
            };

            baProgressModal.open();
            httpDataService.requestData(requestData).then(function (res) {
                if (res.status === 'success') {
                    baProgressModal.close();
                    toastr.info('Измененно');
                    vm.getData();
                }
            }, function () {
                baProgressModal.close();
                toastr.error('Ошибка')

            })
        }
    }
})();

/**
 * @author g0ncharuk
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.home', [])
        .config(routeConfig);


    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('main.home', {
                url: 'home/',
                templateUrl: 'app/pages/home/home.html',
                controller: 'HomeCtrl',
                controllerAs: 'vm',
                title: 'Главная',
                permission: ['handle_admin'],
                visibility: true,
                sidebarMeta: {
                    icon: 'ion-home',
                    order: 100
                }
            })
    }

})();

/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.review', [])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('main.review', {
                url: 'review/',
                templateUrl: 'app/pages/review/review.html',
                controller: 'ReviewCtrl',
                controllerAs: 'vm',
                title: 'Отзывы',
                permission: ['handle_admin'],
                visibility: true,
                sidebarMeta: {
                    icon: 'ion-chatbox',
                    order: 700
                }
            })
            .state('main.reviewAdd', {
                url: 'review/add',
                templateUrl: 'app/pages/review/review-add/review-add.html',
                controller: 'ReviewAddCtrl',
                controllerAs: 'vm',
                title: '',
                permission: ['handle_admin'],
                visibility: false,
                sidebarMeta: {
                    icon: '',
                    order: 0
                }
            })
            .state('main.reviewEdit', {
                url: 'review/edit',
                templateUrl: 'app/pages/review/review-edit/review-edit.html',
                controller: 'ReviewEditCtrl',
                controllerAs: 'vm',
                title: '',
                permission: ['handle_admin'],
                visibility: false,
                sidebarMeta: {
                    icon: '',
                    order: 0
                }
            })
    }

})();

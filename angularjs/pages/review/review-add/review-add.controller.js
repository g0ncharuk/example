/**
 * @author g0ncharuk
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.courseProgram')
        .controller('ReviewAddCtrl', ReviewAddCtrl);

    /** @ngInject */
    function ReviewAddCtrl($scope, $state, toastr, baProgressModal, $uibModal, dataService, httpDataService, Config) {
        var vm = this;

        vm.addAction = addAction;
        vm.goBack = goBack;
        vm.urlTransform = urlTransform;
        vm.token = localStorage.getItem('token');

        vm.canAdd = dataService.canAdd;
        vm.canAdd === null ? vm.goBack() : void(0);


        function urlTransform(url) {
            var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
            var match = url.match(regExp);
            return (match&&match[7].length==11)? "https://www.youtube.com/embed/" + match[7] : url;
        }

        function addAction() {
            var requestData = {
                "url": Config.storeReviews,
                "body": {
                    "token": vm.token,
                    "position": vm._position,
                    "fullname": vm._fullname,
                    "video": vm.urlTransform(vm._video)
                }
            };

            baProgressModal.open();
            httpDataService.requestData(requestData).then(function (res) {
                if (res.status === 'success') {
                    baProgressModal.close();
                    toastr.success('Добавленно');
                    vm.goBack();
                }
            }, function () {
                baProgressModal.close();
                toastr.error('Ошибка')

            })
        }

        function goBack() {
            $state.go('main.review')
        }
    }
})();

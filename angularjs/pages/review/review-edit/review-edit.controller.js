/**
 * @author g0ncharuk
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.review')
        .controller('ReviewEditCtrl', ReviewEditCtrl);

    /** @ngInject */
    function ReviewEditCtrl($scope, $state, toastr, baProgressModal, $uibModal, dataService, httpDataService, Config) {
        var vm = this;

        vm.editAction = editAction;
        vm.goBack = goBack;
        vm.fillForm = fillForm;
        vm.urlTransform = urlTransform;
        vm.token = localStorage.getItem('token');

        vm.editObj = dataService.editID;
        vm.editObj === null ? vm.goBack(): vm.fillForm(vm.editObj);

        function urlTransform(url) {
            var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
            var match = url.match(regExp);
            return (match&&match[7].length==11)? "https://www.youtube.com/embed/" + match[7] : url;
        }

        function fillForm(item) {
            vm._position = Number(item.position);
            vm._fullname = item.fullname;
            vm._video = item.video;
        }

        function editAction() {
            var requestData = {
                "url": Config.storeReviews,
                "body": {
                    "token": vm.token,
                    "id": vm.editObj.id,
                    "position": vm._position,
                    "fullname": vm._fullname,
                    "video": vm.urlTransform(vm._video)
                }
            };

            baProgressModal.open();
            httpDataService.requestData(requestData).then(function (res) {
                if (res.status === 'success') {
                    baProgressModal.close();
                    toastr.info('Изменено');
                    vm.goBack();
                }
            }, function () {
                baProgressModal.close();
                toastr.error('Ошибка')

            })
        }

        function goBack() {
            $state.go('main.review')
        }
    }
})();

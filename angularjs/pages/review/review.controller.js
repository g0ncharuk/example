/**
 * @author g0ncharuk
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.review')
        .controller('ReviewCtrl', ReviewCtrl);

    /** @ngInject */
    function ReviewCtrl($scope, $state, toastr, baProgressModal, $uibModal, dataService, httpDataService, Config) {
        var vm = this;

        vm.editAction = editAction;
        vm.actionPage = actionPage;
        vm.openDeleteModal = openDeleteModal;


        vm.getData = getData;
        vm.token = localStorage.getItem('token');
        dataService.canAdd = true;
        vm.tableSize = 15;

        vm.getData();
        function actionPage() {
            $state.go('main.reviewAdd')
        }

        function getData() {
            var requestData = {
                "url": Config.getReviews,
                "body": {
                    "token": vm.token
                }
            };

            baProgressModal.open();
            httpDataService.requestData(requestData).then(function (res) {
                if (res.status === 'success') {
                    baProgressModal.close();
                    vm.responseData = res.body;
                    vm._responseData = vm.responseData
                }
            }, function () {
                baProgressModal.close();
                toastr.error('Неудачно!')
            });
        }

        function editAction(item) {
            dataService.editID = item;
            $state.go('main.reviewEdit')
        }

        function openDeleteModal(page, size, id) {
            $uibModal.open({
                animation: true,
                templateUrl: page,
                size: size
            }).result.then(function () {
                var requestData = {
                    "url": Config.deleteReviews,
                    "body": {
                        "token": vm.token,
                        "id": id
                    }
                };


                baProgressModal.open();
                httpDataService.requestData(requestData).then(function (res) {
                    if (res.status === 'success') {
                        vm.getData();
                        baProgressModal.close();
                        toastr.error('Удалено')
                    }
                }, function () {
                    baProgressModal.close();
                    toastr.error('Неудачно!')
                });
            })
        }
    }
})();

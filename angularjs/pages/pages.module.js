/**
 * @author g0ncharuk
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages', [
        'ui.router',
        'ui.select',

        'BlurAdmin.pages.main',
        'BlurAdmin.pages.authorization',
        'BlurAdmin.pages.greeting',
        'BlurAdmin.pages.users',
        'BlurAdmin.pages.description',

        // 'BlurAdmin.pages.home',
        // 'BlurAdmin.pages.aboutSpeaker',
        // 'BlurAdmin.pages.aboutCourse',
        // 'BlurAdmin.pages.courseDescription',
        // 'BlurAdmin.pages.courseProgram',
        // 'BlurAdmin.pages.courseStart',
        // 'BlurAdmin.pages.review',
        'BlurAdmin.pages.contacts'


    ])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($urlRouterProvider, baSidebarServiceProvider) {

    }

})();

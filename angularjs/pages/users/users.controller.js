/**
 * @author g0ncharuk
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.users')
        .controller('UsersPageCtrl', UsersPageCtrl);

    /** @ngInject */
    function UsersPageCtrl($scope, $state, toastr, baProgressModal, $uibModal, dataService, httpDataService, Config) {
        var vm = this;

        vm.editAction = editAction;
        vm.fillForm = fillForm;
        vm.getData = getData;
        vm.token = localStorage.getItem('token');

        vm.getData();

        function getData() {
            vm.fillForm()
        }

        function fillForm(item) {
            vm._login = localStorage.getItem('login');
        }

        function editAction() {
            var requestData = {
                "url": Config.updateUsers,
                "body": {
                    "token": vm.token,
                    "login": vm._login,
                    "password": vm._password
                }
            };

            baProgressModal.open();
            httpDataService.requestData(requestData).then(function (res) {
                if (res.status === 'success') {
                    baProgressModal.close();
                    toastr.info('Измененно');
                    localStorage.setItem('login',vm._login);
                }
            }, function () {
                baProgressModal.close();
                toastr.error('Ошибка')

            })
        }
    }
})();

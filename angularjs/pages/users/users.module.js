/**
 * @author g0ncharuk
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.users', [])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('main.users', {
                url: 'access/',
                templateUrl: 'app/pages/users/users.html',
                controller: 'UsersPageCtrl',
                controllerAs: 'vm',
                title: 'Доступ',
                permission: ['handle_admin'],
                visibility: true,
                sidebarMeta: {
                    icon: 'ion-person',
                    order: 100
                }
            })
    }

})();

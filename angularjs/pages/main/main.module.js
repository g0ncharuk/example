/**
 * @author g0ncharuk
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.main', [])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('main', {
                url: '/',
                templateUrl: 'app/pages/main/main.html',
                permission: ['authorized'],
                visibility: false
            });
    }

})();

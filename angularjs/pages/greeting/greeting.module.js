/**
 * @author g0ncharuk
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.greeting', [])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('main.greeting', {
                url: 'greeting',
                templateUrl: 'app/pages/greeting/greeting.html',
                title: '-',
                visibility: false,
                controller: 'GreetingPageCtrl',
                permission: ['authorized'],
                controllerAs: 'vm',
                sidebarMeta: {
                    icon: '',
                    order: 0
                }
            });
    }

})();

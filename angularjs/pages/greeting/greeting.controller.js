/**
 * @author g0ncharuk
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.greeting')
        .controller('GreetingPageCtrl', GreetingPageCtrl);

    /** @ngInject */
    function GreetingPageCtrl($scope) {
        var vm = this;
        vm.userName = localStorage.getItem('login');
        var split_afternoon = 12;
        var split_evening = 17;
        var currentHour = parseFloat(moment().format("HH"));

        if (currentHour >= split_afternoon && currentHour <= split_evening) {
            vm.greeting = "Приветствую, " + vm.userName;
        } else if (currentHour >= split_evening) {
            vm.greeting = "Приветствую," + vm.userName;
        } else {
            vm.greeting = "Приветствую, " + vm.userName;
        }
    }


})();

/**
 * @author g0ncharuk
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.description', [])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('main.description', {
                url: 'description/',
                templateUrl: 'app/pages/description/description.html',
                controller: 'DescriptionCtrl',
                controllerAs: 'vm',
                title: 'Описание',
                permission: ['handle_admin'],
                visibility: true,
                sidebarMeta: {
                    icon: 'ion-document-text',
                    order: 100
                }
            })
    }

})();

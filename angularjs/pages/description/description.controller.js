/**
 * @author g0ncharuk
 */
(function() {
  'use strict';

  angular.module('BlurAdmin.pages.description')
    .controller('DescriptionCtrl', DescriptionCtrl);

  /** @ngInject */
  function DescriptionCtrl($scope, $state, toastr, baProgressModal, $uibModal, dataService, httpDataService, Config, ImgConfig) {
    var vm = this;

    vm.editAction = editAction;
    vm.fillForm = fillForm;
    vm.getData = getData;
    vm.token = localStorage.getItem('token');

    vm.getData();

    function getData() {
      var requestData = {
        "url": Config.getDescription,
        "body": {
          "token": vm.token
        }
      };

      baProgressModal.open();
      httpDataService.requestData(requestData).then(function(res) {
        if (res.status === 'success') {
          baProgressModal.close();
          vm.responseData = res.body;
          vm.fillForm(vm.responseData)
        }
      }, function(error) {
        baProgressModal.close();
        toastr.error('Неудачно!')
      });
    }

    function fillForm(item) {
      vm._about = item.about;
      vm._imagesPreview = _.map(item.ara_photos, function(val) {
        return ImgConfig.url + val.image_url.replace(' ', '%20')
      });
      vm._attraction_text = item.attraction_text;
      vm._genichesk_text = item.genichesk_text;
      vm._image1Preview = ImgConfig.url + item.genichesk_image;
      vm._relax_text = item.relax_text;
      vm._image2Preview = ImgConfig.url + item.relax_image;

      // vm._ara_photos = item.ara_photos;
      // vm._genichesk_image = item.genichesk_image;
      // vm._relax_image = item.relax_image;
    }

    function editAction() {
      var requestData = {
        "url": Config.updateDescription,
        "body": {
          "token": vm.token,
          "about": vm._about,
          "attraction_text": vm._attraction_text,
          "genichesk_text": vm._genichesk_text,
          "relax_text": vm._relax_text,
          "ara_photos": _.map(vm._ara_photos),
          "genichesk_image": vm._genichesk_image[0],
          "relax_image": vm._relax_image[0]
        }
      };
      // baProgressModal.open();
      httpDataService.requestData(requestData).then(function(res) {
        if (res.status === 'success') {
          baProgressModal.close();
          toastr.info('Измененно');
          vm.getData()
        }
      }, function() {
        // baProgressModal.close();
        toastr.error('Ошибка')

      })
    }
  }
})();

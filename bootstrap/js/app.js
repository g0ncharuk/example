(function () {
    "use strict";

    $(document).ready(function () {
        new app.preloader();
        new app.preStart();
    });

    window.onload = function () {
        var loc = window.location.href,
            index = loc.indexOf('#');

        if (index > 0) {
            window.location = loc.substring(0, index);
        }
    };

})();




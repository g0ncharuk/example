'use strict';

var app = app || {};

app.homePage = function () {
    this.initialize();
};

app.homePage.prototype = {
    initialize: function () {
        app.desktop.classList.remove('--no-load');
        app.desktop.classList.add('--is-load');

        if (!app.is_mobile.any()) {
            var videoLoad = app.video.find('#video-home').YTPlayer({
                fitToBackground: true,
                videoId: 'foEBdKzx4uw',
                playerVars: {
                    modestbranding: 0,
                    autoplay: 1,
                    controls: 1,
                    showinfo: 0,
                    branding: 0,
                    rel: 0,
                    autohide: 0
                }
            });

            $(window).mousemove(function (e) {
                var x = e.clientX;
                var y = e.clientY;
                $('#text-paralax-1').css({
                    transform: "translate3d(" + -1 * (x / 200) + "px," + -1 * (0) + "px, 0px)"
                });
                $('#text-paralax-2').css({
                    transform: "translate3d(" + x / 200 + "px," + (0) + "px, 0px)"
                });
            });
        }
    }
};


'use strict';

var app = app || {};


app.window = $(window);
app.body = document.querySelector("body");

app.desktop = document.getElementById("desktop");
app.desktopProj = document.getElementById("desktop-proj");
app.desktopProjType1 = document.getElementById("desktop-proj-type-1");
app.desktopProjType2 = document.getElementById("desktop-proj-type-2");
app.desktopAbout = document.getElementById("desktop-about");
app.desktopServices = document.getElementById("desktop-services");


app.desktopWeb = document.getElementById("desktop-web");
app.desktopDesign = document.getElementById("desktop-design");
app.desktopSeo = document.getElementById("desktop-seo");

app.desktopOrder = document.getElementById("desktop-order");

app._width = window.innerWidth;
app._height = window.innerHeight;

app.preloader_conf = {
    "id": $('#page-preloader'),
    "url": "/assets/images/preloader.gif",
    "width": 500,
    "height": 500,
    "delay": 1200
};

app.logo = $(".logo");

//main layout
app.title1 = $(".fixed-content .title h1");
app.title2 = $(".fixed-content .title h2");
app.title3 = $(".fixed-content .title h3");
app.title4 = $(".fixed-content .title h4");

app.content1 = $(".fixed-content .content p:nth-child(1)");
app.content2 = $(".fixed-content .content p:nth-child(2)");
app.content3 = $(".fixed-content .content p:nth-child(3)");
app.content4 = $(".fixed-content .content p:nth-child(4)");
app.fixFooter = $(".fixed-content-footer");
app.pageFooter = $(".footer-frame");
app.mouseMove = $('#container-content').find('div');

//web layout
app.video = $(".video-container ");
app.threeRender = $("#three-bird");
app.paralaxImgs = $("#aqua-teen-main");
app.commerceImgs = $("#commerce-main");
app.cartImgs = $("#shop-cart");
app.cartGroups = $("#shop-cart-obj").find("g");
app.blickBackgr = $("#aqua-teen-background");
app.mouseMoveInv = app.cartImgs.find('svg');

//design layout

app.branding = $("#branding");
app.designImgs = $("#web-design");

app.creating = $("#creating-logo");

//illustration
app.illustration= $("#illustration");
app.illustrationSeo= $("#illustration-seo");
app.illustrationContext = $("#illustration-context");
app.illustrationTarg = $("#illustration-targ");
app.palitra = $("#palitra");
app.palitraEv = $('.ill-background');

app.threeRenderEarth = $("#three-earth");

app.vizImg = $("#viz-main");

app.granimWrapper = $(".granim-wrapper");













'use strict';

var app = app || {};

app.button = function () {
    this.initialize();
};

app.button.prototype = {
    initialize: function () {
        var mypapers = [];
        document.getElementById('wavecanvas1') ? initPaper(0, document.getElementById('wavecanvas1'), document.getElementById('wavebutton1'), 'black') : null;
        document.getElementById('wavecanvas2') ? initPaper(0, document.getElementById('wavecanvas2'), document.getElementById('wavebutton2'), 'white') : null;

        function initPaper(id, canvasElement, button, fillColor) {
            mypapers[id] = new paper.PaperScope();
            paper = mypapers[id];
            paper.setup(canvasElement);

            var width, height, center, bounds, yPos;
            var points = 8;
            var smooth = true;
            var path = new paper.Path();

            var mouseInside = false;
            var isRunning = false;
            var pushDown = 360;

            var pathHeight = paper.view.size.height / 6;
            var buttonWidth = button.offsetWidth;

            path.fillColor = fillColor;

            //var tool = new paper.Tool();

            paper.view.initializePath = function () {
                center = paper.view.center;
                width = paper.view.size.width;
                height = paper.view.size.height / 2;
                bounds = paper.view.bounds;

                path.segments = [];
                path.add(paper.view.bounds.bottomLeft);

                for (var i = 1; i < points; i++) {
                    var point = new paper.Point(width / points * i, pathHeight + pushDown);
                    path.add(point);
                }
                path.add(paper.view.bounds.bottomRight);
            };

            paper.view.initializePath();

            paper.view.onFrame = function (event) {

                if ((mouseInside || !mouseInside && pushDown < 180)) {

                    for (var i = 1; i < points; i++) {

                        var sinSeed = event.count + (i + i % 10) * 5;
                        var sinHeight = Math.sin(sinSeed / 11) * pathHeight;

                        if (mouseInside && pushDown > 0) {
                            isRunning = true;
                            pushDown -= 1.2;
                        } else if (!mouseInside && pushDown < 180) {
                            isRunning = true;
                            pushDown += 1.2;
                        } else {
                            isRunning = false;
                        }

                        if (isRunning) {
                            yPos = sinHeight + pushDown;
                            path.segments[i].point.y = yPos;
                        }
                    }

                    if (smooth)
                        path.smooth({type: 'continuous'});

                }

            };

            button.onmouseout = function (event) {
                mouseInside = false;
            };

            button.onmouseover = function (event) {
                mouseInside = true;
            }
        }
    }
};


"use strict";
var app = app || {};

app.preStart = function () {
    this.initialize();
};

app.preStart.prototype = {
    initialize: function () {

        //browser
        app.is_chrome = navigator.userAgent.indexOf('Chrome') > -1;
        app.is_explorer = navigator.userAgent.indexOf('MSIE') > -1;
        app.is_firefox = navigator.userAgent.indexOf('Firefox') > -1;
        app.is_safari = navigator.userAgent.indexOf("Safari") > -1;
        app.is_opera = navigator.userAgent.toLowerCase().indexOf("op") > -1;
        if ((app.is_chrome) && (app.is_safari)) {
            app.is_safari = false;
        }
        if ((app.is_chrome) && (app.is_opera)) {
            app.is_chrome = false;
        }

        //is mobile
        app.is_mobile = {
            Android: function () {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function () {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function () {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function () {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function () {
                return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
            },
            any: function () {
                return (app.is_mobile.Android() || app.is_mobile.BlackBerry() || app.is_mobile.iOS() || app.is_mobile.Opera() || app.is_mobile.Windows());
            }
        };

    }
};

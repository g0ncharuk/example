'use strict';

var app = app || {};

app.preloader = function () {
    this.initialize();
};

app.preloader.prototype = {
    initialize: function () {
        app.preloader_conf.id.append("<img src=" + app.preloader_conf.url + ">");
        this.img = app.preloader_conf.id.find("img");
        var img = this.img;
        app.preloader_conf.id.css({
            'position': 'fixed',
            'left': '0',
            'top': '0',
            'width': '100%',
            'height': '100%',
            'overflow': 'visible',
            'background-color': '#fff',
            'z-index': '100500'
        });
        this.img.css({
            'width': app.preloader_conf.width + 'px',
            'height': app.preloader_conf.height + 'px',
            'position': 'absolute',
            'left': '50%',
            'top': '50%',
            'margin': '-' + app.preloader_conf.height / 2 + 'px 0 0 -' + app.preloader_conf.width / 2 + 'px'
        });
        app.preloader_conf.id.delay(app.preloader_conf.delay).fadeOut('slow');
        setTimeout(function () {
            img.removeAttr('src', '');
            new app.Start();


        }, app.preloader_conf.delay + 500);



    }
};


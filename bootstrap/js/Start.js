'use strict';

var app = app || {};

app.Start = function () {
    this.initialize();
};

app.Start.prototype = {
    initialize: function () {

        if (app.desktop) {
            new app.homePage();
        }
        if (app.desktopAbout) {
            new app.aboutPage();
        }
        if (app.desktopServices) {
            new app.servicesPage();
        }
        if (app.desktopProj) {
            new app.projPage();
        }
        if (app.desktopProjType1) {
            new app.projPageType1();
        }
        if (app.desktopProjType2) {
            new app.projPageType2();
        }
        if (app.desktopOrder) {
            new app.orderPage();
        }

        if (app.is_safari) {
            $(".card-small").addClass("active")
        } else {
            $(".card-small").mouseenter(function () {
                $(this).addClass("active")
            }).mouseleave(function () {
                $(this).removeClass("active")
            })
        }

        var footerFixedHeader = $(".fixed-content-header");

        // var headLogo = footerFixedHeader.find(".logo");
        // var headHumburger = footerFixedHeader.find(".hamburger span");
        // var footerFrame = $(".footer-frame");

        // $(window).scroll(function () {
        //     var wScroll = Math.floor($(window).scrollTop());
        //     headLogo.css('color', '#000000');
        //     headHumburger.css('background-color', '#000000');
        //     if (wScroll >= footerFrame.offset().top) {
        //         headLogo.css('color', '#ffffff');
        //         headHumburger.css('background-color', '#ffffff');
        //     }
        // });

        var lastScrollTop = 0;
        $(window).scroll(function () {
            var windowpos = $(window).scrollTop();
            if (!$(".modal").hasClass("in")) {
                if (windowpos > lastScrollTop) {
                    footerFixedHeader.removeClass("is-show")
                } else {
                    footerFixedHeader.addClass("is-show")
                }
            }
            lastScrollTop = windowpos;
        });

        new app.menuWave();
    }
};

